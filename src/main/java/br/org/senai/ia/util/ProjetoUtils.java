package br.org.senai.ia.util;

import br.org.senai.ia.model.Profissional;
import br.org.senai.ia.model.Projeto;

public class ProjetoUtils {

    public static int somarCustoProjeto(Projeto projeto) {
        var ref = new Object() {
            Integer custoProjeto = 0;
        };

        projeto.getModulos().forEach(m -> ref.custoProjeto += m.getProfissionais().stream().mapToInt(Profissional::getRemuneracaoHora).sum());

        return ref.custoProjeto;
    }
}
