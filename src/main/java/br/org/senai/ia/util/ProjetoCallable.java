package br.org.senai.ia.util;

import br.org.senai.ia.heuristica.ProjetoHeuristica;
import br.org.senai.ia.model.Modulo;
import br.org.senai.ia.model.Projeto;
import br.org.senai.ia.repository.ModuloRepository;
import br.org.senai.ia.repository.ProjetoRepository;
import lombok.Builder;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.UUID;
import java.util.concurrent.Callable;

@Slf4j
@Builder
public class ProjetoCallable implements Callable<Projeto> {

    private ProjetoRepository projetoRepository;

    private ModuloRepository moduloRepository;

    private ProjetoHeuristica projetoHeuristica;

    @Override
    public Projeto call() {
        UUID uuid = UUID.randomUUID();
        log.debug("montando projeto {}", uuid);
        long startTime = System.currentTimeMillis();
        Projeto projeto = projetoRepository.criarProjeto(String.valueOf(uuid));
        projetoRepository.adicionarModulos(projeto);
        atribuirProfissionais(projeto.getModulos());
        projeto.setPeso(projetoHeuristica.calcularHeuristica(projeto));
        projetoRepository.addCombinacao(projeto);
        long endTime = System.currentTimeMillis();
        log.debug("Tempo de montagem do projeto {} (em milisegundos): {}", uuid, endTime - startTime);
        return projeto;
    }

    private void atribuirProfissionais(List<Modulo> modulos) {
        modulos.forEach(m -> m.setProfissionais(moduloRepository.criarProfissionaisPorModulo(m, modulos)));
    }
}
