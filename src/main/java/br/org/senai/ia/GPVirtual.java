package br.org.senai.ia;

import br.org.senai.ia.heuristica.ModuloHeuristica;
import br.org.senai.ia.heuristica.ProfissionalHeuristica;
import br.org.senai.ia.heuristica.ProjetoHeuristica;
import br.org.senai.ia.model.Projeto;
import br.org.senai.ia.repository.ModuloRepository;
import br.org.senai.ia.repository.ProfissionalRepository;
import br.org.senai.ia.repository.ProjetoRepository;
import br.org.senai.ia.util.ProjetoCallable;
import br.org.senai.ia.util.ProjetoUtils;
import lombok.extern.slf4j.Slf4j;

import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadLocalRandom;

@Slf4j
class GPVirtual {

    void montarProjetos() throws InterruptedException {

        log.info("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
        log.info("$$                                       Evolucao 0                                   $$");
        log.info("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");

        for (int ex = 0; ex < 5; ex++) {
            log.info("########################################################################################");
            log.info("                                Ciclo {}", ex);
            log.info("########################################################################################");

            int qtdeProfissionais = 1000;
            int qtdeProjetos = 200;
            int qtdeEvolucoes = 100;

            execucao(qtdeProfissionais, qtdeProjetos, qtdeEvolucoes);
        }

        log.info("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
        log.info("$$                                       Evolucao 1                                   $$");
        log.info("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");

        for (int ex = 0; ex < 5; ex++) {
            log.info("########################################################################################");
            log.info("                                Ciclo {}", ex);
            log.info("########################################################################################");

            int qtdeProfissionais = 1000;
            int qtdeProjetos = 500;
            int qtdeEvolucoes = 250;

            execucao(qtdeProfissionais, qtdeProjetos, qtdeEvolucoes);
        }

        log.info("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
        log.info("$$                                       Evolucao 2                                   $$");
        log.info("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
        for (int ex = 0; ex < 5; ex++) {
            log.info("########################################################################################");
            log.info("                                Ciclo {}", ex);
            log.info("########################################################################################");

            int qtdeProfissionais = 1000;
            int qtdeProjetos = 1000;
            int qtdeEvolucoes = 500;

            execucao(qtdeProfissionais, qtdeProjetos, qtdeEvolucoes);
        }

        log.info("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
        log.info("$$                                       Evolucao 3                                   $$");
        log.info("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");

        for (int ex = 0; ex < 5; ex++) {
            log.info("########################################################################################");
            log.info("                                Ciclo {}", ex);
            log.info("########################################################################################");

            int qtdeProfissionais = 50;
            int qtdeProjetos = 10000;
            int qtdeEvolucoes = 1000;

            execucao(qtdeProfissionais, qtdeProjetos, qtdeEvolucoes);
        }
    }

    private void execucao(int qtdeProfissionais, int qtdeProjetos, int qtdeEvolucoes) throws InterruptedException {
        final ExecutorService executorService = Executors.newFixedThreadPool(2);

        final ProfissionalHeuristica profissionalHeuristica = new ProfissionalHeuristica();
        final ModuloHeuristica moduloHeuristica = new ModuloHeuristica(profissionalHeuristica);
        final ProjetoHeuristica projetoHeuristica = new ProjetoHeuristica(moduloHeuristica);

        final ProfissionalRepository profissionalRepository = new ProfissionalRepository(qtdeProfissionais);
        final ModuloRepository moduloRepository = new ModuloRepository(profissionalRepository);
        final ProjetoRepository projetoRepository = new ProjetoRepository(moduloRepository, projetoHeuristica);

        List<ProjetoCallable> callables = new ArrayList<>();
        for (int i = 0; i < qtdeProjetos; i++) {
            ProjetoCallable projetoCallable = ProjetoCallable.builder()
                    .projetoHeuristica(projetoHeuristica)
                    .projetoRepository(projetoRepository)
                    .moduloRepository(moduloRepository)
                    .build();
            callables.add(projetoCallable);
        }
        executorService.invokeAll(callables);
        executorService.shutdown();
        List<Projeto> combinacoesProjetos = new ArrayList<>(projetoRepository.getCombinacoesProjetos());

        Comparator<Projeto> comparator = Comparator.comparing(Projeto::getPeso);
        combinacoesProjetos.sort(comparator.reversed());

        List<Projeto> cuttedArray = combinacoesProjetos.subList(0, (combinacoesProjetos.size() / 2));

        log.info("## fixed position ##");
        evolution(projetoRepository, qtdeEvolucoes, combinacoesProjetos, comparator, cuttedArray, false);

        log.info("## random position ##");
        evolution(projetoRepository, qtdeEvolucoes, combinacoesProjetos, comparator, cuttedArray, true);
    }

    private void evolution(ProjetoRepository projetoRepository, int qtdeEvolucoes, List<Projeto> combinacoesProjetos, Comparator<Projeto> comparator, List<Projeto> cuttedArray, boolean random) {
        Map<Integer, List<Projeto>> evolutionsMap = new HashMap<>();
        for (int e = 0; e < qtdeEvolucoes; e++) {
            List<Projeto> projectsEvolution = new ArrayList<>();
            for (int i = 0; i < cuttedArray.size(); i += 2) {
                if (random) {
                    projectsEvolution.addAll(projetoRepository.mesclarProjetos(cuttedArray.get(i), cuttedArray.get(i + 1), ThreadLocalRandom.current().nextInt(23) + 1));
                } else {
                    projectsEvolution.addAll(projetoRepository.mesclarProjetos(cuttedArray.get(i), cuttedArray.get(i + 1)));
                }
            }
            projectsEvolution.sort(comparator.reversed());

            evolutionsMap.put(e, projectsEvolution);
            cuttedArray = projectsEvolution.subList(0, (projectsEvolution.size() / 2));
        }

        printArranjo(-1, combinacoesProjetos);
        printArranjo(evolutionsMap.size(), evolutionsMap.get(evolutionsMap.size() - 1));
    }

    private void printArranjo(int arranjo, List<Projeto> projetos) {
        Projeto melhorProjeto = projetos.get(0);
        Projeto piorProjeto = projetos.get(projetos.size() - 1);
        log.info("Evolução {} tem a melhor heurística {} com o custo {} e a pior {} com custo {}.", arranjo, melhorProjeto.getPeso(), ProjetoUtils.somarCustoProjeto(melhorProjeto), piorProjeto.getPeso(), ProjetoUtils.somarCustoProjeto(piorProjeto));
        printProjeto(melhorProjeto);
    }

    private void printProjeto(Projeto projeto) {
        log.info("Melhor Projeto: {}.", projeto.getNome());
        log.info("  peso: {}.", projeto.getPeso());
        log.info("  recurso: {}.", projeto.getRecurso());
        log.info("  horas: {}.", projeto.getHoras());
    }
}
