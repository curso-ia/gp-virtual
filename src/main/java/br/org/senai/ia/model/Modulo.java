package br.org.senai.ia.model;

import lombok.*;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class Modulo implements Cloneable {
    private EnumArea area;
    private String nome;
    private Integer cargaHoraria;
    private Integer minColaboradores;
    private Integer maxColaboradores;
    private EnumLinguagem linguagem;
    private EnumDatabase db;
    private List<EnumCargo> cargosMinimos;
    private List<EnumCargo> cargosExcluidos;
    private List<EnumJornada> jornadaObrigatoria;
    private List<Profissional> profissionais;
    private Integer peso;

    public List<EnumCargo> getCargosMinimos() {
        if (cargosMinimos == null) {
            cargosMinimos = new ArrayList<>();
        }
        return cargosMinimos;
    }

    public List<EnumCargo> getCargosExcluidos() {
        if (cargosExcluidos == null) {
            cargosExcluidos = new ArrayList<>();
        }
        return cargosExcluidos;
    }

    public List<EnumJornada> getJornadaObrigatoria() {
        if (jornadaObrigatoria == null) {
            jornadaObrigatoria = new ArrayList<>();
        }
        return jornadaObrigatoria;
    }

    public List<Profissional> getProfissionais() {
        if (profissionais == null) {
            profissionais = new ArrayList<>();
        }
        return profissionais;
    }

    @Override
    public Modulo clone() {
        try {
            Modulo clone = (Modulo) super.clone();
            clone.setProfissionais(new ArrayList<>());
            return clone;
        } catch (CloneNotSupportedException e) {
            log.error(e.getLocalizedMessage());
            return new Modulo();
        }
    }
}
