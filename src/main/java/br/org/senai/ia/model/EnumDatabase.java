package br.org.senai.ia.model;

public enum EnumDatabase {
    POSTGRES(0),
    CASSANDRA(1),
    MONGODB(2),
    COCKROACHDB(3);

    private Integer value;

    EnumDatabase(Integer value) {
        this.value = value;
    }

    public Integer getValue() {
        return this.value;
    }
}
