package br.org.senai.ia.model;

import lombok.*;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class Projeto {
    private String nome;
    private Integer recurso;
    private Integer horas;
    private List<Modulo> modulos;
    private Integer peso;

    public List<Modulo> getModulos() {
        if (modulos == null) {
            modulos = new ArrayList<>();
        }
        return modulos;
    }
}
