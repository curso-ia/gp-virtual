package br.org.senai.ia.model;

public enum EnumArea {
    AREA_0(0), AREA_1(1), AREA_2(2), AREA_3(3), AREA_4(4),
    AREA_5(5), AREA_6(6), AREA_7(7), AREA_8(8), AREA_9(9);

    private Integer value;

    EnumArea(Integer value) {
        this.value = value;
    }

    public Integer getValue() {
        return this.value;
    }
}
