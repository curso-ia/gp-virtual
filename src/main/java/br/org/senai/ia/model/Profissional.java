package br.org.senai.ia.model;

import lombok.*;
import lombok.extern.slf4j.Slf4j;

import java.util.Set;

@Slf4j
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class Profissional implements Cloneable {

    private String nome;
    private Integer remuneracaoHora;
    private EnumCargo cargo;
    private Set<EnumLinguagem> linguagensProgramacao;
    private Set<EnumDatabase> dbs;
    private Set<EnumArea> areasModulos;
    private Integer horasDisponiveis;
    private Integer projetosEmAndamento;
    private EnumJornada tipoJornada;
    private Integer peso;

    @Override
    public Profissional clone() {
        try {
            return (Profissional) super.clone();
        } catch (CloneNotSupportedException e) {
            log.error(e.getLocalizedMessage());
            return new Profissional();
        }
    }
}
