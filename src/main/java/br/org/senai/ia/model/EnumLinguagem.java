package br.org.senai.ia.model;

public enum EnumLinguagem {
    JAVA(0),
    GOLANG(1),
    NODEJS(2),
    PYTHON(3),
    ELIXIR(4),
    RUBY(5);

    private Integer value;

    EnumLinguagem(Integer value) {
        this.value = value;
    }

    public Integer getValue() {
        return this.value;
    }
}
