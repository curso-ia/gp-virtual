package br.org.senai.ia.model;

public enum EnumCargo {
    ANALISTA_JUNIOR(0), ANALISTA_PLENO(1), ANALISTA_SENIOR(2),
    DEV_JUNIOR(3), DEV_PLENO(4), DEV_SENIOR(5),
    DESIGNER(6),
    GP(7),
    COORDENADOR(98);

    private Integer value;

    EnumCargo(Integer value) {
        this.value = value;
    }

    public Integer getValue() {
        return this.value;
    }
}
