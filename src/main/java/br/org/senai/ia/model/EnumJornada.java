package br.org.senai.ia.model;

public enum EnumJornada {
    IN_LOCO(0),
    REMOTO(1);

    private Integer value;

    EnumJornada(Integer value) {
        this.value = value;
    }

    public Integer getValue() {
        return this.value;
    }
}
