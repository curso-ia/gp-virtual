package br.org.senai.ia.repository;

import br.org.senai.ia.model.*;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ThreadLocalRandom;

public class ProfissionalRepository {

    private List<Profissional> profissionais;

    public ProfissionalRepository(int tamanho) {
        inicializarProfissionais(tamanho);
    }

    public void inicializarProfissionais(int tamanho) {
        profissionais = new ArrayList<>();
        for (int i = 0; i < tamanho; i++) {
            profissionais.add(gerarProfissional(i));
        }
    }

    public List<Profissional> getProfissionais() {
        return profissionais;
    }

    private Profissional gerarProfissional(int index) {
        return Profissional.builder()
                .nome("Profissional " + index)
                .remuneracaoHora((int) (((Math.random() + 1) * 1000 * ((Math.random() * 5)))))
                .cargo(gerarCargo())
                .linguagensProgramacao(gerarLinguagens())
                .dbs(gerarDBs())
                .areasModulos(gerarAreas())
                .horasDisponiveis(gerarHorasDisponiveis())
                .projetosEmAndamento(gerarProjetosEmAndamento())
                .tipoJornada(gerarJornada())
                .build();
    }

    private EnumCargo gerarCargo() {
        return EnumCargo.values()[ThreadLocalRandom.current().nextInt(9)];
    }

    private Set<EnumLinguagem> gerarLinguagens() {
        Set<EnumLinguagem> linguagens = new HashSet<>();

        for (int i = 0; i < ThreadLocalRandom.current().nextInt(5) + 1; i++) {
            linguagens.add(EnumLinguagem.values()[ThreadLocalRandom.current().nextInt(6)]);
        }

        return linguagens;
    }

    private Set<EnumDatabase> gerarDBs() {
        Set<EnumDatabase> dbs = new HashSet<>();

        for (int i = 0; i < ThreadLocalRandom.current().nextInt(3) + 1; i++) {
            dbs.add(EnumDatabase.values()[ThreadLocalRandom.current().nextInt(4)]);
        }

        return dbs;
    }

    private Set<EnumArea> gerarAreas() {
        Set<EnumArea> areas = new HashSet<>();

        for (int i = 0; i < ThreadLocalRandom.current().nextInt(4); i++) {
            areas.add(EnumArea.values()[ThreadLocalRandom.current().nextInt(10)]);
        }

        return areas;
    }

    private Integer gerarHorasDisponiveis() {
        return ThreadLocalRandom.current().nextInt(200) + 1;
    }

    private Integer gerarProjetosEmAndamento() {
        return ThreadLocalRandom.current().nextInt(4) + 1;
    }

    private EnumJornada gerarJornada() {
        return EnumJornada.values()[ThreadLocalRandom.current().nextInt(2)];
    }
}
