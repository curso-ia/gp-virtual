package br.org.senai.ia.repository;

import br.org.senai.ia.model.*;

import java.util.*;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;

public class ModuloRepository {

    private List<Modulo> modulos;

    private ProfissionalRepository profissionalRepository;

    public ModuloRepository(ProfissionalRepository profissionalRepository) {
        this.profissionalRepository = profissionalRepository;
        inicializarModulos();
    }

    public void inicializarModulos() {
        modulos = new ArrayList<>();
        Modulo mod00 = Modulo.builder()
                .area(EnumArea.AREA_0)
                .nome("Modulo 0")
                .cargaHoraria(80 * 2)
                .minColaboradores(2)
                .maxColaboradores(6)
                .linguagem(EnumLinguagem.JAVA)
                .db(EnumDatabase.POSTGRES)
                .cargosMinimos(Arrays.asList(EnumCargo.ANALISTA_PLENO, EnumCargo.DEV_PLENO))
                .cargosExcluidos(Arrays.asList(EnumCargo.ANALISTA_SENIOR, EnumCargo.DEV_SENIOR, EnumCargo.GP))
                .jornadaObrigatoria(Collections.singletonList(EnumJornada.IN_LOCO))
                .build();
        modulos.add(mod00);

        Modulo mod01 = Modulo.builder()
                .area(EnumArea.AREA_1)
                .nome("Modulo 1")
                .cargaHoraria(150 * 3)
                .minColaboradores(3)
                .maxColaboradores(8)
                .linguagem(EnumLinguagem.JAVA)
                .db(EnumDatabase.POSTGRES)
                .cargosMinimos(Arrays.asList(EnumCargo.ANALISTA_PLENO, EnumCargo.DEV_PLENO))
                .jornadaObrigatoria(Collections.singletonList(EnumJornada.IN_LOCO))
                .build();
        modulos.add(mod01);

        Modulo mod02 = Modulo.builder()
                .area(EnumArea.AREA_1)
                .nome("Modulo 2")
                .cargaHoraria(50 * 2)
                .minColaboradores(2)
                .maxColaboradores(5)
                .linguagem(EnumLinguagem.JAVA)
                .db(EnumDatabase.POSTGRES)
                .cargosExcluidos(Arrays.asList(EnumCargo.ANALISTA_PLENO, EnumCargo.ANALISTA_SENIOR, EnumCargo.DEV_PLENO, EnumCargo.DEV_SENIOR, EnumCargo.GP))
                .jornadaObrigatoria(Arrays.asList(EnumJornada.IN_LOCO, EnumJornada.REMOTO))
                .build();
        modulos.add(mod02);

        Modulo mod03 = Modulo.builder()
                .area(EnumArea.AREA_1)
                .nome("Modulo 3")
                .cargaHoraria(100 * 3)
                .minColaboradores(3)
                .maxColaboradores(5)
                .linguagem(EnumLinguagem.JAVA)
                .db(EnumDatabase.POSTGRES)
                .cargosMinimos(Arrays.asList(EnumCargo.ANALISTA_PLENO, EnumCargo.DEV_PLENO))
                .jornadaObrigatoria(Arrays.asList(EnumJornada.IN_LOCO, EnumJornada.REMOTO))
                .build();
        modulos.add(mod03);

        Modulo mod04 = Modulo.builder()
                .area(EnumArea.AREA_2)
                .nome("Modulo 4")
                .cargaHoraria(150 * 3)
                .minColaboradores(3)
                .maxColaboradores(8)
                .linguagem(EnumLinguagem.JAVA)
                .db(EnumDatabase.MONGODB)
                .cargosMinimos(Arrays.asList(EnumCargo.ANALISTA_SENIOR, EnumCargo.DEV_SENIOR))
                .jornadaObrigatoria(Collections.singletonList(EnumJornada.IN_LOCO))
                .build();
        modulos.add(mod04);

        Modulo mod05 = Modulo.builder()
                .area(EnumArea.AREA_2)
                .nome("Modulo 5")
                .cargaHoraria(150 * 2)
                .minColaboradores(2)
                .maxColaboradores(8)
                .linguagem(EnumLinguagem.NODEJS)
                .db(EnumDatabase.MONGODB)
                .cargosMinimos(Arrays.asList(EnumCargo.ANALISTA_SENIOR, EnumCargo.DEV_SENIOR))
                .jornadaObrigatoria(Collections.singletonList(EnumJornada.IN_LOCO))
                .build();
        modulos.add(mod05);

        Modulo mod06 = Modulo.builder()
                .area(EnumArea.AREA_2)
                .nome("Modulo 6")
                .cargaHoraria(80 * 2)
                .minColaboradores(2)
                .maxColaboradores(5)
                .linguagem(EnumLinguagem.JAVA)
                .db(EnumDatabase.MONGODB)
                .cargosMinimos(Arrays.asList(EnumCargo.ANALISTA_SENIOR, EnumCargo.DEV_SENIOR))
                .jornadaObrigatoria(Arrays.asList(EnumJornada.IN_LOCO, EnumJornada.REMOTO))
                .build();
        modulos.add(mod06);

        Modulo mod07 = Modulo.builder()
                .area(EnumArea.AREA_2)
                .nome("Modulo 7")
                .cargaHoraria(80 * 2)
                .minColaboradores(2)
                .maxColaboradores(5)
                .linguagem(EnumLinguagem.PYTHON)
                .db(EnumDatabase.MONGODB)
                .cargosMinimos(Arrays.asList(EnumCargo.ANALISTA_SENIOR, EnumCargo.DEV_SENIOR))
                .jornadaObrigatoria(Arrays.asList(EnumJornada.IN_LOCO, EnumJornada.REMOTO))
                .build();
        modulos.add(mod07);

        Modulo mod08 = Modulo.builder()
                .area(EnumArea.AREA_3)
                .nome("Modulo 8")
                .cargaHoraria(200 * 4)
                .minColaboradores(4)
                .maxColaboradores(8)
                .linguagem(EnumLinguagem.GOLANG)
                .db(EnumDatabase.MONGODB)
                .cargosMinimos(Arrays.asList(EnumCargo.ANALISTA_SENIOR, EnumCargo.DEV_SENIOR))
                .jornadaObrigatoria(Arrays.asList(EnumJornada.IN_LOCO, EnumJornada.REMOTO))
                .build();
        modulos.add(mod08);

        Modulo mod09 = Modulo.builder()
                .area(EnumArea.AREA_4)
                .nome("Modulo 9")
                .cargaHoraria(100)
                .minColaboradores(1)
                .maxColaboradores(4)
                .linguagem(EnumLinguagem.ELIXIR)
                .db(EnumDatabase.COCKROACHDB)
                .cargosMinimos(Arrays.asList(EnumCargo.ANALISTA_SENIOR, EnumCargo.DEV_SENIOR))
                .jornadaObrigatoria(Arrays.asList(EnumJornada.IN_LOCO, EnumJornada.REMOTO))
                .build();
        modulos.add(mod09);

        Modulo mod10 = Modulo.builder()
                .area(EnumArea.AREA_6)
                .nome("Modulo 10")
                .cargaHoraria(150 * 4)
                .minColaboradores(4)
                .maxColaboradores(8)
                .linguagem(EnumLinguagem.JAVA)
                .db(EnumDatabase.POSTGRES)
                .cargosMinimos(Arrays.asList(EnumCargo.ANALISTA_SENIOR, EnumCargo.DEV_SENIOR))
                .jornadaObrigatoria(Arrays.asList(EnumJornada.IN_LOCO, EnumJornada.REMOTO))
                .build();
        modulos.add(mod10);

        Modulo mod11 = Modulo.builder()
                .area(EnumArea.AREA_6)
                .nome("Modulo 11")
                .cargaHoraria(150 * 4)
                .minColaboradores(4)
                .maxColaboradores(8)
                .linguagem(EnumLinguagem.JAVA)
                .db(EnumDatabase.POSTGRES)
                .cargosMinimos(Arrays.asList(EnumCargo.ANALISTA_SENIOR, EnumCargo.DEV_SENIOR))
                .jornadaObrigatoria(Arrays.asList(EnumJornada.IN_LOCO, EnumJornada.REMOTO))
                .build();
        modulos.add(mod11);

        Modulo mod12 = Modulo.builder()
                .area(EnumArea.AREA_6)
                .nome("Modulo 12")
                .cargaHoraria(150 * 4)
                .minColaboradores(4)
                .maxColaboradores(8)
                .linguagem(EnumLinguagem.JAVA)
                .db(EnumDatabase.POSTGRES)
                .cargosMinimos(Arrays.asList(EnumCargo.ANALISTA_SENIOR, EnumCargo.DEV_SENIOR))
                .jornadaObrigatoria(Arrays.asList(EnumJornada.IN_LOCO, EnumJornada.REMOTO))
                .build();
        modulos.add(mod12);

        Modulo mod13 = Modulo.builder()
                .area(EnumArea.AREA_6)
                .nome("Modulo 13")
                .cargaHoraria(150 * 4)
                .minColaboradores(4)
                .maxColaboradores(8)
                .linguagem(EnumLinguagem.JAVA)
                .db(EnumDatabase.POSTGRES)
                .cargosMinimos(Arrays.asList(EnumCargo.ANALISTA_SENIOR, EnumCargo.DEV_SENIOR))
                .jornadaObrigatoria(Arrays.asList(EnumJornada.IN_LOCO, EnumJornada.REMOTO))
                .build();
        modulos.add(mod13);

        Modulo mod14 = Modulo.builder()
                .area(EnumArea.AREA_6)
                .nome("Modulo 14")
                .cargaHoraria(150 * 4)
                .minColaboradores(4)
                .maxColaboradores(8)
                .linguagem(EnumLinguagem.JAVA)
                .db(EnumDatabase.POSTGRES)
                .cargosMinimos(Arrays.asList(EnumCargo.ANALISTA_SENIOR, EnumCargo.DEV_SENIOR))
                .jornadaObrigatoria(Arrays.asList(EnumJornada.IN_LOCO, EnumJornada.REMOTO))
                .build();
        modulos.add(mod14);

        Modulo mod15 = Modulo.builder()
                .area(EnumArea.AREA_6)
                .nome("Modulo 15")
                .cargaHoraria(150)
                .minColaboradores(4)
                .maxColaboradores(8)
                .linguagem(EnumLinguagem.JAVA)
                .db(EnumDatabase.POSTGRES)
                .cargosMinimos(Arrays.asList(EnumCargo.ANALISTA_SENIOR, EnumCargo.DEV_SENIOR))
                .jornadaObrigatoria(Arrays.asList(EnumJornada.IN_LOCO, EnumJornada.REMOTO))
                .build();
        modulos.add(mod15);

        Modulo mod16 = Modulo.builder()
                .area(EnumArea.AREA_6)
                .nome("Modulo 16")
                .cargaHoraria(150 * 4)
                .minColaboradores(4)
                .maxColaboradores(8)
                .linguagem(EnumLinguagem.JAVA)
                .db(EnumDatabase.POSTGRES)
                .cargosMinimos(Arrays.asList(EnumCargo.ANALISTA_SENIOR, EnumCargo.DEV_SENIOR))
                .jornadaObrigatoria(Arrays.asList(EnumJornada.IN_LOCO, EnumJornada.REMOTO))
                .build();
        modulos.add(mod16);

        Modulo mod17 = Modulo.builder()
                .area(EnumArea.AREA_6)
                .nome("Modulo 17")
                .cargaHoraria(150 * 4)
                .minColaboradores(4)
                .maxColaboradores(8)
                .linguagem(EnumLinguagem.JAVA)
                .db(EnumDatabase.POSTGRES)
                .cargosMinimos(Arrays.asList(EnumCargo.ANALISTA_SENIOR, EnumCargo.DEV_SENIOR))
                .jornadaObrigatoria(Arrays.asList(EnumJornada.IN_LOCO, EnumJornada.REMOTO))
                .build();
        modulos.add(mod17);

        Modulo mod18 = Modulo.builder()
                .area(EnumArea.AREA_6)
                .nome("Modulo 18")
                .cargaHoraria(150 * 4)
                .minColaboradores(4)
                .maxColaboradores(8)
                .linguagem(EnumLinguagem.JAVA)
                .db(EnumDatabase.POSTGRES)
                .cargosMinimos(Arrays.asList(EnumCargo.ANALISTA_SENIOR, EnumCargo.DEV_SENIOR))
                .jornadaObrigatoria(Arrays.asList(EnumJornada.IN_LOCO, EnumJornada.REMOTO))
                .build();
        modulos.add(mod18);

        Modulo mod19 = Modulo.builder()
                .area(EnumArea.AREA_6)
                .nome("Modulo 19")
                .cargaHoraria(150 * 4)
                .minColaboradores(4)
                .maxColaboradores(8)
                .linguagem(EnumLinguagem.JAVA)
                .db(EnumDatabase.POSTGRES)
                .cargosMinimos(Arrays.asList(EnumCargo.ANALISTA_SENIOR, EnumCargo.DEV_SENIOR))
                .jornadaObrigatoria(Arrays.asList(EnumJornada.IN_LOCO, EnumJornada.REMOTO))
                .build();
        modulos.add(mod19);

        Modulo mod20 = Modulo.builder()
                .area(EnumArea.AREA_6)
                .nome("Modulo 20")
                .cargaHoraria(150 * 4)
                .minColaboradores(4)
                .maxColaboradores(8)
                .linguagem(EnumLinguagem.JAVA)
                .db(EnumDatabase.POSTGRES)
                .cargosMinimos(Arrays.asList(EnumCargo.ANALISTA_SENIOR, EnumCargo.DEV_SENIOR))
                .jornadaObrigatoria(Arrays.asList(EnumJornada.IN_LOCO, EnumJornada.REMOTO))
                .build();
        modulos.add(mod20);

        Modulo mod21 = Modulo.builder()
                .area(EnumArea.AREA_6)
                .nome("Modulo 21")
                .cargaHoraria(150 * 4)
                .minColaboradores(4)
                .maxColaboradores(8)
                .linguagem(EnumLinguagem.JAVA)
                .db(EnumDatabase.POSTGRES)
                .cargosMinimos(Arrays.asList(EnumCargo.ANALISTA_SENIOR, EnumCargo.DEV_SENIOR))
                .jornadaObrigatoria(Arrays.asList(EnumJornada.IN_LOCO, EnumJornada.REMOTO))
                .build();
        modulos.add(mod21);

        Modulo mod22 = Modulo.builder()
                .area(EnumArea.AREA_6)
                .nome("Modulo 22")
                .cargaHoraria(150 * 4)
                .minColaboradores(4)
                .maxColaboradores(8)
                .linguagem(EnumLinguagem.JAVA)
                .db(EnumDatabase.POSTGRES)
                .cargosMinimos(Arrays.asList(EnumCargo.ANALISTA_SENIOR, EnumCargo.DEV_SENIOR))
                .jornadaObrigatoria(Arrays.asList(EnumJornada.IN_LOCO, EnumJornada.REMOTO))
                .build();
        modulos.add(mod22);

        Modulo mod23 = Modulo.builder()
                .area(EnumArea.AREA_7)
                .nome("Modulo 23")
                .cargaHoraria(150 * 4)
                .minColaboradores(4)
                .maxColaboradores(8)
                .linguagem(EnumLinguagem.JAVA)
                .db(EnumDatabase.POSTGRES)
                .cargosMinimos(Arrays.asList(EnumCargo.ANALISTA_SENIOR, EnumCargo.DEV_SENIOR))
                .jornadaObrigatoria(Arrays.asList(EnumJornada.IN_LOCO, EnumJornada.REMOTO))
                .build();
        modulos.add(mod23);

        Modulo mod24 = Modulo.builder()
                .area(EnumArea.AREA_8)
                .nome("Modulo 24")
                .cargaHoraria(150 * 4)
                .minColaboradores(4)
                .maxColaboradores(8)
                .linguagem(EnumLinguagem.JAVA)
                .db(EnumDatabase.POSTGRES)
                .cargosMinimos(Arrays.asList(EnumCargo.ANALISTA_SENIOR, EnumCargo.DEV_SENIOR))
                .jornadaObrigatoria(Arrays.asList(EnumJornada.IN_LOCO, EnumJornada.REMOTO))
                .build();
        modulos.add(mod24);

        Modulo mod25 = Modulo.builder()
                .area(EnumArea.AREA_9)
                .nome("Modulo 25")
                .cargaHoraria(150 * 4)
                .minColaboradores(4)
                .maxColaboradores(8)
                .linguagem(EnumLinguagem.JAVA)
                .db(EnumDatabase.POSTGRES)
                .cargosMinimos(Arrays.asList(EnumCargo.ANALISTA_SENIOR, EnumCargo.DEV_SENIOR))
                .jornadaObrigatoria(Arrays.asList(EnumJornada.IN_LOCO, EnumJornada.REMOTO))
                .build();
        modulos.add(mod25);
    }

    public List<Modulo> getModulos() {
        return modulos;
    }

    public List<Profissional> criarProfissionaisPorModulo(Modulo modulo, List<Modulo> modulos) {
        List<Profissional> profissionais = new ArrayList<>();

        int maxColaboradores = ThreadLocalRandom.current().nextInt(modulo.getMaxColaboradores());
        if (maxColaboradores < modulo.getMinColaboradores()) {
            maxColaboradores = modulo.getMinColaboradores();
        }

        for (int i = 0; i < maxColaboradores; i++) {
            List<Profissional> profissionaisHabilitados = profissionalRepository.getProfissionais()
                    .parallelStream().filter(p -> isProfissionalHabilitado(modulo, p, modulos))
                    .collect(Collectors.toList());

            Profissional profissional = profissionaisHabilitados.get(ThreadLocalRandom.current().nextInt(profissionaisHabilitados.size())).clone();
            profissionais.add(profissional);

            if (isAtingiuMaxHorasModulo(modulo, profissionais)) {
                break;
            }
        }

        return profissionais;
    }

    private boolean isAtingiuMaxHorasModulo(Modulo modulo, List<Profissional> profissionais) {
        Integer horasProfissionais = profissionais.stream().mapToInt(Profissional::getHorasDisponiveis).sum();
        return modulo.getCargaHoraria() < horasProfissionais;
    }

    private boolean isProfissionalHabilitado(Modulo modulo, Profissional profissional, List<Modulo> modulos) {
        boolean profissionalHabilitado = !modulo.getCargosExcluidos().contains(profissional.getCargo())
                && profissional.getLinguagensProgramacao().contains(modulo.getLinguagem())
                && profissional.getDbs().contains(modulo.getDb())
                && modulo.getJornadaObrigatoria().contains(profissional.getTipoJornada());
        var ref = new Object() {
            Map<String, Long> ocorrenciasProfissional;
        };
        modulos.parallelStream().forEach(m -> {
            ref.ocorrenciasProfissional = m.getProfissionais().stream().collect(Collectors.groupingBy(Profissional::getNome, Collectors.counting()));
        });

        return profissionalHabilitado
                && (!ref.ocorrenciasProfissional.containsKey(profissional.getNome()) || ref.ocorrenciasProfissional.get(profissional.getNome()) > 2);
    }
}
