package br.org.senai.ia.repository;

import br.org.senai.ia.heuristica.ProjetoHeuristica;
import br.org.senai.ia.model.Projeto;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class ProjetoRepository {

    private ModuloRepository moduloRepository;

    private ProjetoHeuristica projetoHeuristica;

    private List<Projeto> combinacoesProjetos = new ArrayList<>();

    public ProjetoRepository(ModuloRepository moduloRepository, ProjetoHeuristica projetoHeuristica) {
        this.moduloRepository = moduloRepository;
        this.projetoHeuristica = projetoHeuristica;
    }

    public Projeto criarProjeto(String sequence) {
        return Projeto.builder()
                .nome("Projeto " + sequence)
                .recurso(380000)
                .horas(300)
                .build();
    }

    public void adicionarModulos(Projeto projeto) {
        moduloRepository.getModulos().forEach(m -> projeto.getModulos().add(m.clone()));
    }

    public List<Projeto> mesclarProjetos(Projeto projetoA, Projeto projetoB, int posicaoCorte) {

        List<Projeto> projetosMesclados = new ArrayList<>();

        Projeto novoProjetoA = criarProjeto("A:" + ThreadLocalRandom.current().nextInt(1000000));
        novoProjetoA.getModulos().addAll(projetoA.getModulos().subList(0, posicaoCorte));
        novoProjetoA.getModulos().addAll(projetoB.getModulos().subList(posicaoCorte, 26));
        novoProjetoA.setPeso(projetoHeuristica.calcularHeuristica(novoProjetoA));
        projetosMesclados.add(novoProjetoA);

        Projeto novoProjetoB = criarProjeto("B:" + ThreadLocalRandom.current().nextInt(1000000));
        novoProjetoB.getModulos().addAll(projetoB.getModulos().subList(0, posicaoCorte));
        novoProjetoB.getModulos().addAll(projetoA.getModulos().subList(posicaoCorte, 26));
        novoProjetoB.setPeso(projetoHeuristica.calcularHeuristica(projetoB));
        projetosMesclados.add(novoProjetoB);

        projetosMesclados.add(projetoA);
        projetosMesclados.add(projetoB);

        return projetosMesclados;
    }

    public List<Projeto> mesclarProjetos(Projeto projetoA, Projeto projetoB) {
        return mesclarProjetos(projetoA, projetoB, 13);
    }

    public List<Projeto> getCombinacoesProjetos() {
        return combinacoesProjetos;
    }

    public void setCombinacoesProjetos(List<Projeto> combinacoesProjetos) {
        this.combinacoesProjetos = combinacoesProjetos;
    }

    public synchronized void addCombinacao(Projeto projeto) {
        combinacoesProjetos.add(projeto);
    }
}
