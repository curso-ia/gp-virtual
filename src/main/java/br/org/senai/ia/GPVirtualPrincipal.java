package br.org.senai.ia;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class GPVirtualPrincipal {

    public static void main(String[] args) throws InterruptedException {
        long startTime = System.currentTimeMillis();

        GPVirtual gpVirtual = new GPVirtual();
        gpVirtual.montarProjetos();
        long endTime = System.currentTimeMillis();
        log.info("Tempo de execucao (em segundos): {}", (endTime - startTime) / 1000);
    }
}
