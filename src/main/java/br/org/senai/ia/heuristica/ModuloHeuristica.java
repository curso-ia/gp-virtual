package br.org.senai.ia.heuristica;

import br.org.senai.ia.model.Modulo;

import java.util.List;

public class ModuloHeuristica {

    private ProfissionalHeuristica profissionalHeuristica;

    public ModuloHeuristica(ProfissionalHeuristica profissionalHeuristica) {
        this.profissionalHeuristica = profissionalHeuristica;
    }

    public int calcularHeuristica(List<Modulo> modulos) {
        var ref = new Object() {
            int peso = 0;
        };

        modulos.forEach(m -> {
            m.setPeso(profissionalHeuristica.calcularHeuristica(m));
            ref.peso += m.getPeso();
        });

        return ref.peso;
    }
}
