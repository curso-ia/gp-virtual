package br.org.senai.ia.heuristica;

import br.org.senai.ia.model.Projeto;
import br.org.senai.ia.util.ProjetoUtils;

public class ProjetoHeuristica {

    private ModuloHeuristica moduloHeuristica;

    public ProjetoHeuristica(ModuloHeuristica moduloHeuristica) {
        this.moduloHeuristica = moduloHeuristica;
    }

    public int calcularHeuristica(Projeto projeto) {
        if (ProjetoUtils.somarCustoProjeto(projeto) > projeto.getRecurso()) {
            return -100;
        }
        return moduloHeuristica.calcularHeuristica(projeto.getModulos());
    }

}
