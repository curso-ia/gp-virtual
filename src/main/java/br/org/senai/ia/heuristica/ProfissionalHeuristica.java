package br.org.senai.ia.heuristica;

import br.org.senai.ia.model.EnumJornada;
import br.org.senai.ia.model.Modulo;
import br.org.senai.ia.model.Profissional;

public class ProfissionalHeuristica {

    public int calcularHeuristica(Modulo modulo) {
        var ref = new Object() {
            int peso = 0;
        };

        modulo.getProfissionais().forEach(p -> {
            ref.peso += pesoTrabalhouNaArea(p, modulo);
            ref.peso += pesoTipoJornada(p);
            ref.peso += pesoCargo(p);
        });

        return ref.peso;
    }

    private int pesoTrabalhouNaArea(Profissional profissional, Modulo modulo) {
        if (profissional.getAreasModulos().contains(modulo.getArea())) {
            return 3;
        }
        return -3;
    }

    private int pesoTipoJornada(Profissional profissional) {
        if (EnumJornada.IN_LOCO.equals(profissional.getTipoJornada())) {
            return 3;
        }
        return 1;
    }

    private int pesoCargo(Profissional profissional) {
        switch (profissional.getCargo()) {
            case DEV_SENIOR:
                return 3;
            case DEV_PLENO:
                return 1;
            case DEV_JUNIOR:
                return -1;
            case ANALISTA_SENIOR:
                return 3;
            case ANALISTA_PLENO:
                return 1;
            case ANALISTA_JUNIOR:
                return -1;
            case DESIGNER:
                return 5;
            case GP:
                return 3;
            case COORDENADOR:
                return -5;
            default:
                return 0;
        }
    }
}
